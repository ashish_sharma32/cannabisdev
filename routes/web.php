<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', [ 'as'=> 'home', 'uses'=> 'PageController@home']);
Route::get('/about', [ 'as'=> 'about', 'uses'=> 'PageController@about']);
Route::get('/products', [ 'as'=> 'products', 'uses'=> 'PageController@products']);
Route::get('/products/{product}', [ 'as'=> 'products.show', 'uses'=> 'PageController@showProducts']);
Route::get('/my-account', [ 'as'=> 'my-account', 'uses'=> 'PageController@myAccount']);
Route::post('user/login', [ 'as'=> 'user.login', 'uses'=> 'UserController@login']);
Route::post('user/store', [ 'as'=> 'user.store', 'uses'=> 'UserController@store']);
Route::put('/user/{user}', [ 'as'=> 'user.update', 'uses'=> 'UserController@update']);
Route::put('/updatepass/{id}', [ 'as'=> 'user.updatepass', 'uses'=> 'UserController@updatePassword']);
Route::post('/checkmail', [ 'as'=> 'checkmail', 'uses'=> 'UserController@checkmail']);
Route::get('user/logout', [ 'as'=> 'user.logout', 'uses'=> 'UserController@logout']);
Route::get('register/verify/{token}', ['as' => 'confirm_email', 'uses' => 'UserController@confirmMail']);
Route::get('/search', [ 'as'=> 'products.search', 'uses'=> 'ProductController@search']);
Route::post('/filterproducts', ['as'=>'products_filter.loadAjax','uses'=>'ProductController@filterProducts']);
Route::post('/getproducts', ['as'=>'products.loadAjax','uses'=>'ProductController@loadProducts']);
Route::get('/add-to-cart/{id}', ['as' => 'products.addToCart', 'uses' => 'ProductController@getAddToCart']);
Route::get('/cart', ['as' => 'products.cart', 'uses' => 'ProductController@showCart']);
Route::get('/get-cart', ['as' => 'products.getCart', 'uses' => 'ProductController@getCart']);
Route::get('/remove-cart/{slug}', ['as' => 'products.deleteFromCart', 'uses' => 'ProductController@deleteFromCart']);
Route::get('/checkout', ['as' => 'products.checkout', 'uses' => 'ProductController@checkout']);
Route::post('/checkout', ['as' => 'products.postCheckout', 'uses' => 'ProductController@postCheckout']);
Route::get('/order-complete/{token}', ['as' => 'products.orderSuccess', 'uses' => 'ProductController@orderSuccess']);
Route::post('/orders/customer', ['as' => 'orders.storeOrderCustomer', 'uses' => 'OrderController@storeOrderCustomer']);
Route::get('auth/{provider}', 'AuthController@redirectToProvider');
Route::get('auth/{provider}/callback', 'AuthController@handleProviderCallback');
Route::group(['middleware' => ['checkifadmin'] , 'prefix' => 'admin'], function () {
	Route::get('dashboard', [ 'as'=> 'admin.dashboard', 'uses'=> 'PageController@dashboard']);
	Route::resource('users','UserController');
	Route::resource('categories','CategoryController');
	Route::resource('products','ProductController');
	Route::resource('orders','OrderController');
	Route::resource('tasks','TaskController');
	Route::resource('drivers','DriverController');
});
Route::group(['middleware' => ['checkifadmin']], function () {
});
