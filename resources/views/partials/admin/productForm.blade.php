<div class="form-group xs-pt-10">
  <label>Name</label>
  {!!Form::text('name',null, array('class' => 'form-control'))!!}
</div>

<div class="form-group">
  <label>Amount($)</label>
  {!!Form::number('price',null, array('class' => 'form-control'))!!}
</div>

<div class="form-group">
  <label>Category</label>
  {{ Form::select('category_id', $categories,null,['id'=> 'category','class' => 'table-group-action-input form-control input-medium categoryId', 'required']) }}
</div>