<!--Fonts-->
<link href='https://fonts.googleapis.com/css?family=Lato:400,700,900,400italic,700italic,300,300italic' rel='stylesheet' type='text/css'>
<link rel='stylesheet' id='thb-google-fonts-css'  href='https://fonts.googleapis.com/css?family=Josefin+Sans%3A300%2C400%2C500%2C600%2C700%2C900&#038;subset=latin%2Clatin-ext&#038;ver=4.6.1' type='text/css' media='all' />
<!--Styles-->
{!! Html::style('site/css/base.css') !!}
{!! Html::style('site/css/skins/square/grey.css') !!}
{!! Html::style('https://cdn.jsdelivr.net/sweetalert2/6.3.0/sweetalert2.min.css') !!}
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
{!! Html::style('site/css/custom.css') !!}