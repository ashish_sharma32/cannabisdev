@extends('layouts.main')
@section('title') Page Not Found - Cannabis Delivery System @stop
@section('content')
<section class="parallax-window" id="short">
    <div id="subheader">
        <div id="sub_content">
            <h1>Page Not Accessible</h1>
            <h1></h1>
            <p>Oops!, Forbidden Request</p>
            <p></p>
        </div>
    </div>
</section>
</div>
@stop