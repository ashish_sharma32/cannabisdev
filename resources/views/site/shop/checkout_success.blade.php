@extends('layouts.main') 
@section('title') Order Complete - Cannabis Delivery System @stop

@section('content')
<section class="parallax-window" id="short">
    <div id="subheader">
        <div id="sub_content">
         <h1>Order Complete</h1>
            <div class="bs-wizard">
              <div class="col-xs-4 bs-wizard-step complete">
                <div class="text-center bs-wizard-stepnum"><strong>1.</strong> Your details</div>
                <div class="progress"><div class="progress-bar"></div></div>
                <a href="cart.html" class="bs-wizard-dot"></a>
              </div>              
              <div class="col-xs-4 bs-wizard-step complete">
                <div class="text-center bs-wizard-stepnum"><strong>2.</strong> Payment</div>
                <div class="progress"><div class="progress-bar"></div></div>
                <a href="cart_2.html" class="bs-wizard-dot"></a>
              </div>
            <div class="col-xs-4 bs-wizard-step complete">
                <div class="text-center bs-wizard-stepnum"><strong>3.</strong> Finish!</div>
                <div class="progress"><div class="progress-bar"></div></div>
                <a href="#0" class="bs-wizard-dot"></a>
              </div>  
            </div>
        </div>
    </div>
</section>

<div class="container margin_60_35">
    <div class="row">
        <div class="col-md-offset-3 col-md-6">
            <div class="box_style_2">
                <h2 class="inner">Order confirmed!</h2>
                <div id="confirm">
                    <i class="icon_check_alt2"></i>
                    <h3>Thank you!</h3>
                    <p>Your Order is Successful.</p> 
                    <p>Your Stripe Transaction ID is <strong>{{$token}}</strong></p>
                </div>
                <h4>Summary</h4>

                <table class="table table-striped nomargin">
                <tbody>
                @foreach($products as $product)
                    <tr>
                      <td>
                      <strong>{{$product['qty']}}x</strong> {{$product['item']->name}}
                      </td>
                      <td>
                          <strong class="pull-right">${{$product['price']}}</strong>
                      </td>
                    </tr>
                    @endforeach
                <tr>
                    <td class="total_confirm">
                      TOTAL
                    </td>
                    <td class="total_confirm">
                      <span class="pull-right">${{$totalPrice}}</span>
                    </td>
                </tr>
                <p>You can check more details by going to your <a href="/my-account">Account Page</a> </p>
                </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@stop