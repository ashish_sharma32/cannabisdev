@extends('layouts.main') 
@section('title') Products - Cannabis Delivery System @stop 
@section('content')
<!--Content-->
<section class="parallax-window" id="short">
    <div id="subheader">
        <div id="sub_content">
            <h1>Products</h1>
        </div>
    </div>
</section>

<div class="collapse" id="collapseMap">
    <div id="map" class="map"></div>
</div>

<div class="container margin_60_35">
    <div class="row">

        <div class="col-md-3">
            <div id="filters_col">
                <a data-toggle="collapse" href="#collapseFilters" aria-expanded="false" aria-controls="collapseFilters" id="filters_col_bt">Filters <i class="icon-plus-1 pull-right"></i></a>
                <div class="collapse in" id="collapseFilters">
                    <div class="filter_type">
                        <h6>Categories</h6>
                        <ul>
                        @if(count($categories)>0)
                            @foreach($categories as $category)
                            <li>
                                <label>
                                <input type="radio" name="filter_cat" class="icheck" data-id="{{$category->id}}" >{{$category->name}}
                                </label>
                            </li>
                            @endforeach @else
                            <p>No Category</p>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-9">

            <div id="tools">
                <div class="row">
                    <div class="col-md-12">
                        <div class="styled-select">
                            <select name="sort_rating" id="sort_rating">
                                <option value="" selected>Sort by ranking</option>
                                <option value="lower">Lowest ranking</option>
                                <option value="higher">Highest ranking</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sk-circle">
              <div class="sk-circle1 sk-child"></div>
              <div class="sk-circle2 sk-child"></div>
              <div class="sk-circle3 sk-child"></div>
              <div class="sk-circle4 sk-child"></div>
              <div class="sk-circle5 sk-child"></div>
              <div class="sk-circle6 sk-child"></div>
              <div class="sk-circle7 sk-child"></div>
              <div class="sk-circle8 sk-child"></div>
              <div class="sk-circle9 sk-child"></div>
              <div class="sk-circle10 sk-child"></div>
              <div class="sk-circle11 sk-child"></div>
              <div class="sk-circle12 sk-child"></div>
            </div>
            <div id="productContainer" class="row">
                @if(!$products->isEmpty()) @foreach ($products as $product)
                <div class="col-md-6 col-sm-6 wow zoomIn" data-wow-delay="0.1s">
                    <a class="strip_list grid" href="/products/{{$product->slug}}">
                        <div class="desc">
                            <div class="thumb_strip">
                                <img src="{{$product->image}}" alt="">
                            </div>
                            <div class="rating">
                                <i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star"></i>
                            </div>
                            <h3>{{$product->name}}</h3>
                            <div class="type">
                                {{$product->category->name}}
                            </div>
                            <div>
                                <h4>${{$product->price}}</h4>
                            </div>
                            <div class="location">
                                {!! $product->description !!}
                            </div>
                            <ul>
                                <li>Delivery<i class="icon_check_alt2 ok"></i></li>
                            </ul>
                        </div>
                    </a>
                </div>
                @endforeach 
                @else
                <h3 style="text-align: center">No Products in the Store</h3> @endif
            </div>
            @if(!$products->isEmpty())
            <button id="btn-more" class="load_more_bt wow fadeIn" data-wow-delay="0.2s" data-id="{{$product->id}}">Load More</button>
            @endif
        </div>
    </div>
</div>
@stop
@section('page-specific-scripts')

<script type="text/javascript">    
    $spinner = "<div class='spinner'><div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div>";
    catID = null;  
    $('input').on('ifChecked', function(event){
        catID = $(this).data('id');
    });
    $(document).on('click', '#btn-more', function(event) {
        $proID = $(this).data('id'); 
        $("#btn-more").html($spinner);
        $.ajax({
            type: "post",
            url: "/getproducts",
            data: {
                'proID': $proID,
                'catID': catID,
                '_token': '{{csrf_token()}}',
            },
            success: function(data) {
                if (!$.trim(data)) {
                    $("#btn-more").html("No More Products");
                    $("#btn-more").removeAttr('data-id');
                } else {
                    $("#productContainer").append(data.products);
                    $("#btn-more").data("id", data.id);
                    $("#btn-more").html("Load More");
                }
            }
        });
    });
    $('input').on('ifChecked', function(event){
      var id = $(this).data('id');  
      $("#productContainer").empty();
      $(".sk-circle").css('display', 'block');
      $.ajax({
        type: "post",
        url: "/filterproducts",
        data: {
                'id': id,
                '_token': '{{csrf_token()}}',
            },
        success: function(data){
            if(data != ''){    
                $('.sk-circle').css('display', 'none');
                $("#btn-more").data("id", data.id);
                $("#productContainer").append(data.products);
                $("#btn-more").html("Load More");
            }
            else{
                $('.sk-circle').css('display', 'none');
                $("#productContainer").append('<h1>No Products Matched For Selected Filter</h1>');
                $("#btn-more").removeAttr('data-id');   
            }
        }
      });
    });
</script>
@stop