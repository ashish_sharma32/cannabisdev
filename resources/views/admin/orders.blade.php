@extends('layouts.admin')
@section('title') Orders Management - Cannabis Delivery System @stop
@section('page-title')Orders Management @stop
@section('page-content')
<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-default panel-table">
      <div class="panel-heading">
        <p>Orders Management</p>
      </div>
      <div class="panel-body">
        <div class="table-responsive noSwipe">
          <table class="table table-striped table-hover">
          @if(count($orders)>0)
            <thead>
              <tr>
                <th class="actions">Order ID</th>
                <th class="actions">User ID</th>
                <th class="actions">Order Date</th>
                <th class="actions">No. of Products</th>
                <th class="actions">Customer</th>
                <th class="actions">Status</th>
              </tr>
            </thead>
            <tbody>
              @foreach($orders as $order)
              <tr>
                <td class="actions"><a href="/admin/orders/{{$order->id}}">{{$order->id}}</a></td>
                <td class="actions">{{$order->user_id}}</td>
                <td class="actions">{{$order->created_at->toDateString()}}</td>
                <td class="actions">{{$order->totalQty}}</td>    
                <td class="actions">{{$order->firstname}} {{$order->lastname}}</td>
                <td class="actions">{{$order->orderstatus->status}}</td>
              </tr>
              @endforeach
            </tbody>
            @else
            <h3 style="text-align: center">No Orders</h3>
          @endif
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@stop