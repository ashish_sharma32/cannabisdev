@extends('layouts.admin')
@section('title') Delivery Tasks Management - Cannabis Delivery System @stop
@section('page-title') Delivery Tasks Management @stop
@section('page-content')
<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-default panel-table">
      <div class="panel-heading">
        <p>Delivery Tasks Management</p>
      </div>
      <div class="panel-body">
        <div class="table-responsive noSwipe">
          <table class="table table-striped table-hover">
            @if(count($tasks)>0)
            <thead>
              <tr> 
                <th class="actions">Order ID</th>
                <th class="actions">Task ID</th>
                <th class="actions">Task Description</th>
                <th class="actions">Customer</th>
                <th class="actions">Shipping Address</th>
              </tr>
            </thead>
            <tbody>
              @foreach($tasks as $task)
              <tr>
                <td class="actions"><button class="viewTask" data-id="{{$task->order_id}}">{{$task->order_id}}</button></td>
                <td class="actions">{{$task->job_id}}</td>
                <td class="actions">{{$task->job_description}}</td>
                <td class="actions">{{$task->customer_username}}</td>
                <td class="actions">{{$task->job_address}}</td>
              </tr>
              @endforeach
            </tbody>
            @else
            <h3 style="text-align: center">No Tasks</h3>
            @endif
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="taskView" tabindex="-1" role="dialog" class="modal fade colored-header colored-header-primary">
  <div class="modal-dialog custom-width">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close"><span class="mdi mdi-close"></span></button>
        <h3 class="modal-title">Task ID : <span class="task-title"></span></h3>
      </div>
      <div class="modal-body">
      <div class="row">
            <div class="col-sm-12">
              <div class="panel panel-default panel-border-color panel-border-color-primary">
                <div class="panel-body">
                  <fieldset disabled>
                    <form class="form-horizontal">
                      <div class="form-group xs-mt-10">
                        <label for="status" class="col-sm-3 control-label">Status</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control job_state">
                        </div>
                      </div>
                      <div class="form-group xs-mt-10">
                        <label for="status" class="col-sm-3 control-label">Remarks</label>
                        <div class="col-sm-9">
                          <textarea class="form-control job_state_desc"></textarea>
                          <!-- <input type="text" class="form-control job_state_desc"> -->
                        </div>
                      </div>
                      <div class="form-group xs-mt-10">
                        <label for="deliver_before" class="col-sm-3 control-label">Deliver Before</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control job_delivery_datetime">
                        </div>
                      </div>
                      <div class="form-group xs-mt-10">
                        <label for="tracking_link" class="col-sm-3 control-label">Tracking Link</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" value="Unseen">
                        </div>
                      </div>
                      <div class="form-group xs-mt-10">
                        <label for="tracking_link" class="col-sm-3 control-label">Customer Name</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control customer_username">
                        </div>
                      </div>
                      <div class="form-group xs-mt-10">
                        <label for="tracking_link" class="col-sm-3 control-label">Customer Contact</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control customer_phone">
                        </div>
                      </div>
                      <div class="form-group xs-mt-10">
                        <label for="tracking_link" class="col-sm-3 control-label">Customer E-Mail</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control customer_email">
                        </div>
                      </div>
                      <div class="form-group xs-mt-10">
                        <label for="tracking_link" class="col-sm-3 control-label">Customer Address</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control job_address">
                        </div>
                      </div>
                    </form>  
                  </fieldset>
                </div>
              </div>
            </div>
          </div>
    </div>
  </div>
</div>
@stop
@section('page-specific-scripts')
{!! Html::script('admin/assets/js/app.js') !!}
@stop
