@extends('layouts.admin')
@section('title') Drivers Management - Cannabis Delivery System @stop 
@section('page-title') Products Management @stop 
@section('page-content')
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default panel-table">
            <div class="panel-heading">
                <div class="col-md-8 col-sm-12 col-xs-12">
                    <p>Drivers Management</p>
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12" style="text-align: right;">
                    <button class="btn btn-info" style="width: 100%"><i class="mdi mdi-plus" style="margin: 0 10px 0 0"></i>ADD NEW DRIVER</button>
                </div>
            </div>
            <div class="panel-body">
              <table class="table">
              @if(count($drivers)>0)
                <thead>
                  <tr> 
                    <th class="actions">Driver Name</th>
                    <th class="actions">Driver Email</th>
                    <th class="actions">Transport Type</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($drivers as $driver)
                    <tr>
                      <td class="actions">{{$driver->username}}</td>
                      <td class="actions">{{$driver->email}}</td>
                      <td class="actions">{{$driver->transport_type}}</td>
                    </tr>
                  @endforeach
                </tbody>
              @else
                <div class="col-md-12">
                <h3 style="text-align: center">There are no Drivers in the System.</h3>
                </div>
              @endif
              </table>
            </div>
        </div>
    </div>
</div>
@stop