@extends('layouts.admin')
@section('title') Product Management - Cannabis Delivery System @stop 
@section('page-title') Product Management @stop 
@section('page-content')
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default panel-table">
            <div class="panel-heading">
                <div class="col-md-8 col-sm-12 col-xs-12">
                    <p>Products Management</p>
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <button class="btn btn-info addEntityBtn">ADD NEW PRODUCT</button>
                </div>
            </div>
            <div class="panel-body">
              <table class="table">
              @if(count($products)>0)
                <thead>
                    <tr>
                        <th style="width:50%;">Product Title</th>
                        <th class="actions">View</th>
                        <th class="actions">Edit</th>
                        <th class="actions">Delete</th>
                    </tr>
                </thead>
                <tbody>
                  
                  @foreach ($products as $product)
                  <tr>
                    <td>{{$product->name}}</td>
                    <td class="actions"><a class="icon viewEntityBtn" data-id="{{$product->id}}" data-entity="{{$entity}}"><i class="mdi mdi-eye"></i></a></td>
                    <td class="actions"><a class="icon editProduct" data-id="{{$product->id}}"><i class="mdi mdi-edit"></i></a></td>
                    <td class="actions">
                        {!! Form::open(['method' => 'DELETE', 'route' => ['products.destroy', $product->id] ,'class' => 'removeForm', 'data-id' => $product->id, 'data-entity' => $entity]) !!}
                        {!! Form::button('Remove', ['class' => 'btn btn-danger rmv','data']) !!}
                        {!! Form::close() !!}
                    </td>
                  </tr>
                  @endforeach                  
                </tbody>
              @else
                <div class="col-md-12">
                <h3 style="text-align: center">No Products</h3>
                </div>
              @endif
              </table>
            </div>
        </div>
    </div>
</div>

<div id="addEntity" tabindex="-1" role="dialog" class="modal fade colored-header colored-header-primary">
    <div class="modal-dialog custom-width">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close"><span class="mdi mdi-close"></span></button>
                <h3 class="modal-title">Product Details</h3>
            </div>
            <div class="modal-body">
                {!! Form::open(array('route' => 'products.store','class' => 'popup-form', 'id'=>'addForm', 'files' => true,'data-entity' => $entity)) !!} 
                @if (count($errors) > 0)
                <div class="alert alert-danger error-alert">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                @include('partials.admin.productForm')
                <div class="form-group xs-pt-10">
                    <label>Description</label>
                    {!!Form::textarea('description',null, array('class' => 'form-control', 'id' => 'descCreate', 'req'))!!}
                </div>
                <div class="form-group">
                    <label>Image</label>
                    {{ Form::file('image', ['class' => 'form-control', 'required']) }}
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-space btn-primary">Submit</button>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>

<div id="productView" tabindex="-1" role="dialog" class="modal fade colored-header colored-header-primary">
    <div class="modal-dialog custom-width">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close"><span class="mdi mdi-close"></span></button>
                <h3 class="modal-title">Product Details</h3>
            </div>
            <div class="modal-body">
                {{ Form::open()}}
                {{ Form::open(array('id'=>'viewEntityForm')) }} 
                <fieldset disabled>
                    @include('partials.admin.productForm')
                    <div class="form-group xs-pt-10">
                        <label>Description</label>
                        {!!Form::textarea('description',null, array('class' => 'form-control' ,'id'=>'descView'))!!}
                    </div>
                    <div class="form-group">
                        <label>Image</label>
                        <img src="" id="productImage" class="img-responsive" style="width: 25%">
                    </div>
                </fieldset>
                {{ Form::close() }}
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-default md-close">Cancel</button>
            </div>
        </div>
    </div>
</div>

<div id="productEdit" tabindex="-1" role="dialog" class="modal fade colored-header colored-header-primary">
    <div class="modal-dialog custom-width">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close"><span class="mdi mdi-close"></span></button>
                <h3 class="modal-title">Edit Product Details</h3>
            </div>
            <div class="modal-body">
                {{ Form::open(array('method' => 'PUT', 'class' => 'popup-form editForm', 'files' => true )) }} 
                @if (count($errors) > 0)
                <div class="alert alert-danger error-alert">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif 
                @include('partials.admin.productForm')
                <div class="form-group xs-pt-10">
                    <label>Description</label>
                    {!!Form::textarea('description',null, array('class' => 'form-control' ,'id'=>'descEdit'))!!}
                </div>
                <div class="form-group">
                    <label>Image</label>
                    <img src="" id="productImage" class="img-responsive" style="width: 25%">
                </div>
                <div class="form-group">
                    <label>Upload A New Image</label>
                    {!! Form::file('image', null, array('class' => 'form-control', 'required')) !!}
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-space btn-primary">Update</button>
            </div>
            {!! Form::close()!!}
        </div>
    </div>
</div>
@stop
@section('page-specific-scripts')
<script type="text/javascript">
$("select.categoryId").flexselect();
tinymce.init({
    selector: '#descCreate',
    menubar: false,
    statusbar: false,
    toolbar: false,
    readonly: false
});
tinymce.init({
    selector: '#descView',   
    menubar: false,
    statusbar: false,
    toolbar: false
});
tinymce.init({
    selector: '#descEdit',
    menubar: false,
    statusbar: false, 
    toolbar: false
});
addForm = $("#addForm");
addForm.validate({
    rules: {
        name: {
            required: !0,
            minlength: 3
        },
        price: {
            required: !0,
            minlength: 3
        }
    }
});
 
// $(".viewEntity").click(function() {
//     $id = $(this).data("id");
//     $entity = $(this).data("entity");
//     $name = $("input[name ='name']");
//     $amount = $("input[name ='price']");
//     $image = $("img#productImage");
//     $.ajax({
//         type: "get",
//         url: "/admin/"+$entity+"/"+$id,
//         success: function(e) {
//             $product = e;
//             $('option[value="' + $product.category_id + '"]').attr("selected", "selected");
//             $name.val($product.name);
//             tinymce.get("descView").setContent($product.description);
//             $amount.val($product.price);
//             $image.attr("src", $product.image);
//             $("#productView").modal("show");
//         }
//     });
// });

</script>

{!! Html::script('admin/assets/js/rmv_entity.js') !!}
{!! Html::script('admin/assets/js/view_entity.js') !!}
{!! Html::script('admin/assets/js/add_entity.js') !!}
@stop