<!DOCTYPE html>
<!--[if IE 9]><html class="ie ie9"><![endif]-->
<html>
<head>
@include('partials.site.meta')
@include('partials.site.styles') 
@include('partials.site.polyfills')
</head>
<body>
    <!--[if lte IE 8]>
        <p class="chromeframe">You are using an <strong>outdated</strong> browser. 
        Please <a href="http://browsehappy.com/">upgrade your browser</a>.</p>
    <![endif]-->
    <header>
        <div class="container-fluid">
            <div class="row">
                <div class="col--md-4 col-sm-4 col-xs-4">
                    <a href="/" id="logo" >
                        {{ Html::image('site/img/logo.png', '', array('class' => 'hidden-xs' , 'data-retina' => 'true',  'width' => '190' , 'height' =>'23' )) }}
                        {{ Html::image('site/img/logo_mobile.png', '', array('class' => 'hidden-lg hidden-md hidden-sm' , 'data-retina' => 'true', 'width' => '59' , 'height' =>'23' )) }}
                    </a>
                </div>
                <nav class="col--md-8 col-sm-8 col-xs-8">
                    <a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a>
                    <a href="/cart" class="mbl-cart"><i class="icon_cart_alt"></i><span class="cartCount badge">{{ Session::has('cart') ? Session::get('cart')->totalQty : null }}</span></a>
                    <div class="main-menu">
                        <div id="header_menu">
                            {{ Html::image('site/img/logo.png', '', array('data-retina' => 'true',  'width' => '190' , 'height' =>'23' )) }}
                        </div>
                        <a href="#" class="open_close" id="close_in"><i class="icon_close"></i></a>
                        <ul>
                            <li><a href="/">Home</a></li>
                            <li><a href="/about">About</a></li>
                            <li><a href="/products">Products</a></li>
                            @if (Auth::check())
                            <li><a href="/my-account">My Account</a></li>
                            <li><a href="{{URL::route('user.logout')}}">Log Out</a></li>
                            @else
                                <li><a href="#0" data-toggle="modal" data-target="#login">Login</a></li>
                                <li><a href="#0" data-toggle="modal" data-target="#register">Register</a></li>
                                
                            @endif
                            <li class="submenu-cart">
                                <a href="/cart" class="show-submenu"><i class="icon_cart_alt"></i><span class="cartCount badge">{{ Session::has('cart') ? Session::get('cart')->totalQty : null }}</span></a>
                                <ul>
                                    <li class="cartItems">
                                    <p style="margin: 0px"></p>
                                    <table class="table table_summary cart_table">
                                        <tbody class="cart-widget">
                                        </tbody>
                                    </table>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </header>
    @yield('content')
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-3">
                    <h3>Secure payments with</h3>
                    <p>
                        {{ Html::image('site/img/cards.png', '', array('class' => 'img-responsive')) }}
                    </p>
                </div>
                <div class="col-md-3 col-sm-3">
                    <h3>About</h3>
                    <ul>
                        <li><a href="/about">About us</a></li>
                        <li><a href="/faq">Faq</a></li>
                        <li><a href="/contacts">Contacts</a></li>
                        <li><a href="/terms">Terms and conditions</a></li>
                    </ul>
                </div>
                <div class="col-md-5 col-sm-6" id="newsletter">
                    <h3>Newsletter</h3>
                    <p>
                        Join our newsletter to keep be informed about offers and news.
                    </p>
                    <div id="message-newsletter_2">
                    </div>
                    <form method="post" action="assets/newsletter.php" name="newsletter_2" id="newsletter_2">
                        <div class="form-group">
                            <input name="email_newsletter_2" id="email_newsletter_2" type="email" value="" placeholder="Your mail" class="form-control">
                        </div>
                        <input type="submit" value="Subscribe" class="btn_1" id="submit-newsletter_2">
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div id="social_footer">
                        <ul>
                            <li><a href="#0"><i class="icon-facebook"></i></a></li>
                            <li><a href="#0"><i class="icon-twitter"></i></a></li>
                            <li><a href="#0"><i class="icon-google"></i></a></li>
                            <li><a href="#0"><i class="icon-instagram"></i></a></li>
                            <li><a href="#0"><i class="icon-pinterest"></i></a></li>
                            <li><a href="#0"><i class="icon-vimeo"></i></a></li>
                            <li><a href="#0"><i class="icon-youtube-play"></i></a></li>
                        </ul>
                        <p>© Cannabis Delivery System 2017</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    @include('partials.site.loginForm')
    @include('partials.site.registerForm')
    @include('partials.site.scripts')
</body>
</html>



