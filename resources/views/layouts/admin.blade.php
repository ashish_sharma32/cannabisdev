<!DOCTYPE html>
<html lang="en">
  <head>
  @include('partials.admin.meta')
  @include('partials.admin.styles') 
  @include('partials.admin.polyfills')
  </head>
  <body>
    <div class="be-wrapper">
      <nav class="navbar navbar-default navbar-fixed-top be-top-header">
        <div class="container-fluid">
          <div class="be-right-navbar">
            <ul class="nav navbar-nav navbar-right be-user-nav">
              <li class="dropdown"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle">
              {{ HTML::image('admin/assets/img/avatar.png', 'Avatar') }}<span class="user-name">{{Auth::user()->firstname}} {{Auth::user()->lastname}}</span></a>
                <ul role="menu" class="dropdown-menu">
                  <li>
                    <div class="user-info">
                      <div class="user-name">{{Auth::user()->firstname}} {{Auth::user()->lastname}}</div>
                    </div>
                  </li>
                  {{-- <li><a href="#"><span class="icon mdi mdi-face"></span> Account</a></li>
                  <li><a href="#"><span class="icon mdi mdi-settings"></span> Settings</a></li> --}}
                  <li><a href="{{URL::route('user.logout')}}"><span class="icon mdi mdi-power"></span> Logout</a></li>
                </ul>
              </li>
            </ul>
            <div class="page-title"><span>Cannabis Delivery System</span></div>
            <ul class="nav navbar-nav navbar-right be-icons-nav">
              <li class="dropdown"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle"><span class="icon mdi mdi-notifications"></span><span class="indicator"></span></a>
                <ul class="dropdown-menu be-notifications">
                  <li>
                    <div class="title">Notifications<span class="badge">3</span></div>
                    <div class="list">
                      <div class="be-scroller">
                        <div class="content">
                          <ul>
                            <li class="notification notification-unread"><a href="#">
                                <div class="image">
                                {{ HTML::image('admin/assets/img/avatar2.png', 'Avatar 2') }}
                                </div>
                                <div class="notification-info">
                                  <div class="text"><span class="user-name">Jessica Caruso</span> accepted your invitation to join the team.</div><span class="date">2 min ago</span>
                                </div></a></li>
                            <li class="notification"><a href="#">
                                <div class="image">
                                {{ HTML::image('admin/assets/img/avatar3.png', 'Avatar 3') }}
                                </div>
                                <div class="notification-info">
                                  <div class="text"><span class="user-name">Joel King</span> is now following you</div><span class="date">2 days ago</span>
                                </div></a></li>
                            <li class="notification"><a href="#">
                                <div class="image">
                                {{ HTML::image('admin/assets/img/avatar4.png', 'Avatar 4') }}
                                </div>
                                <div class="notification-info">
                                  <div class="text"><span class="user-name">John Doe</span> is watching your main repository</div><span class="date">2 days ago</span>
                                </div></a></li>
                            <li class="notification"><a href="#">
                                <div class="image">
                                {{ HTML::image('admin/assets/img/avatar5.png', 'Avatar 5') }}
                                </div>
                                <div class="notification-info"><span class="text"><span class="user-name">Emily Carter</span> is now following you</span><span class="date">5 days ago</span></div></a></li>
                          </ul>
                        </div>
                      </div>
                    </div>
                    <div class="footer"> <a href="#">View all notifications</a></div>
                  </li>
                </ul>
              </li>
          </div>
        </div>
      </nav>
      <div class="be-left-sidebar">
        <div class="left-sidebar-wrapper"><a href="#" class="left-sidebar-toggle">@yield('page-title')</a>
          <div class="left-sidebar-spacer">
            <div class="left-sidebar-scroll">
              <div class="left-sidebar-content">
                <ul class="sidebar-elements">
                  <li class="divider">Menu</li>
                  <li><a href="{{URL::route('admin.dashboard')}}"><i class="icon mdi mdi-home"></i><span>Dashboard</span></a></li>
                  <li><a href="{{URL::route('users.index')}}"><i class="icon mdi mdi-account"></i><span>User Management</span></a></li>
                  <li><a href="{{URL::route('products.index')}}"><i class="icon mdi mdi-shopping-basket"></i><span>Products</span></a>
                  </li>
                  <li><a href="{{URL::route('categories.index')}}"><i class="icon mdi mdi-tag"></i><span>Categories</span></a>
                  </li>
                  <li><a href="{{URL::route('orders.index')}}"><i class="icon mdi mdi-shopping-cart"></i><span>Orders</span></a>
                  </li>
                  <li><a href="{{URL::route('tasks.index')}}"><i class="icon mdi mdi-shopping-cart"></i><span>Delivery Tasks</span></a>
                  </li>
                  <li><a href="{{URL::route('drivers.index')}}"><i class="icon mdi mdi-bike"></i><span>Drivers</span></a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="be-content">
        <div class="page-head">
          <h2 class="page-head-title">@yield('page-title')</h2>
          <ol class="breadcrumb page-head-nav">
            <li><a href="{{URL::route('admin.dashboard')}}">Home</a></li>
            <li class="active">@yield('page-title')</li>
          </ol>
        </div>
        <div class="main-content container-fluid">
          @yield('page-content')
        </div>
      </div>
    </div>
    @include('partials.admin.scripts')    
  </body>
</html>