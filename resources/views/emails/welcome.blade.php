<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <title>Welcome Mail</title>
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,300,500,600,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Oswald:400,300,700" rel="stylesheet" type="text/css">
    <style type="text/css">
        @import url(https://fonts.googleapis.com/css?family=Poppins:400,300,500,600,700);
        @import url(https://fonts.googleapis.com/css?family=Oswald:400,300,700);
        [class="full"] {
            margin: 0 auto
        }
        
        [class="fullw"] {
            margin: 0 auto
        }
        
        img {
            margin: 0;
            border: none
        }
        
        a {
            text-decoration: none;
            text-align: left;
            font-size: 14px;
            font-family: Poppins, arial;
            font-weight: 300;
            line-height: 15px;
            color: #878787
        }
        
        @media only screen and (min-width: 515px) and (max-width: 860px) {
            [class="fullw"] {
                width: 530px !important;
                margin: 0 auto
            }
            [class="full"] {
                width: 500px !important;
                margin: 0 auto
            }
            [class="full-no"] {
                width: 500px !important;
                margin: 0 auto;
                background: none !important
            }
            [class="process-text"] {
                width: 343px !important
            }
            [class="stat"] {
                width: 158px !important
            }
            [class="service-text"] {
                width: 383px !important
            }
            [class="work-sp"] p {
                padding-left: 15px !important
            }
            [class="work"] {
                width: 148px !important
            }
            [class="work"] img {
                width: 100% !important;
                height: auto !important
            }
            [class="rtext-top"] {
                height: 90px !important
            }
            [class="rtext-top2"] {
                height: 50px !important
            }
            [class="member"] {
                width: 158px !important
            }
            [class="price-text"] {
                width: 267px !important;
                margin-right: 15px !important
            }
            [class="price-text"] td {
                font-size: 13px !important
            }
            [class="award"] {
                width: 158px !important
            }
            [class="award"] img {
                width: 100% !important;
                height: auto !important
            }
            [class="faq-text"] {
                width: 483px !important
            }
            [class="blog-img"] {
                width: 241px !important
            }
            [class="blog-img"] img {
                width: 100% !important;
                height: auto !important
            }
            [class="blog-text"] {
                width: 226px !important
            }
            [class="blog-text"] br {
                display: none !important
            }
            [class="col2"] {
                width: 231px !important
            }
            [class="col2"] img {
                width: 100% !important;
                height: auto !important
            }
            [class="col2"] br {
                display: none !important
            }
            [class="contact"] {
                width: 158px !important
            }
        }
        
        @media only screen and (min-width: 510px) and (max-width: 660px) {
            [class="lpic"] {
                width: 100% !important
            }
            [class="rtext"] {
                width: 100% !important
            }
            [class="rtext-in"] {
                width: 400px !important;
                margin: 0 auto !important;
                float: none !important
            }
            [class="rtext-top"] {
                height: 50px !important
            }
            [class="rtext-top2"] {
                height: 50px !important
            }
            [class="rtext-botp"] {
                height: 50px !important
            }
            [class="rtext-botp2"] {
                height: 50px !important
            }
            [class="rtext-sp"] {
                display: none !important
            }
            [class="client-text"] {
                width: 365px !important
            }
        }
        
        @media only screen and (max-width: 520px) {
            table {
                clear: both
            }
            [class="full"] {
                width: 280px !important
            }
            [class="full-no"] {
                width: 280px !important;
                margin: 0 auto;
                background: none !important
            }
            [class="fullw"] {
                width: 280px !important
            }
            [class="menu-sp"] {
                width: 10px !important
            }
            [class="logo"] {
                float: none !important;
                margin: 0 auto
            }
            table[class="menu"] {
                float: none !important;
                margin: 0 auto
            }
            [class="process-number"] {
                width: 280px !important;
                margin-bottom: 20px !important
            }
            [class="process-text"] {
                width: 280px !important
            }
            [class="process-text"] td {
                text-align: center !important
            }
            [class="service-icon"] {
                width: 280px !important;
                margin-bottom: 20px !important
            }
            [class="service-text"] {
                width: 280px !important
            }
            [class="service-text"] td {
                text-align: center !important
            }
            [class="stat"] {
                width: 280px !important;
                margin-bottom: 20px !important
            }
            [class="work"] {
                width: 280px !important;
                margin-bottom: 20px !important
            }
            [class="work"] img {
                margin: 0 auto
            }
            [class="worknone"] {
                height: 20px !important
            }
            [class="fullw"] {
                width: 280px !important
            }
            [class="lpic"] {
                width: 100% !important
            }
            [class="rtext"] {
                width: 100% !important
            }
            [class="rtext-in"] {
                width: 280px !important;
                margin: 0 auto !important;
                float: none !important
            }
            [class="rtext-in"] td {
                text-align: center !important
            }
            [class="rtext-in"] table {
                margin: 0 auto !important;
                float: none !important
            }
            [class="rtext-top"] {
                height: 50px !important
            }
            [class="rtext-top2"] {
                height: 50px !important
            }
            [class="rtext-botp"] {
                height: 50px !important
            }
            [class="rtext-botp2"] {
                height: 50px !important
            }
            [class="rtext-sp"] {
                display: none !important
            }
            [class="member"] {
                width: 280px !important;
                margin-bottom: 20px !important
            }
            [class="price"] {
                width: 280px !important;
                margin-bottom: 5px !important
            }
            [class="price-text"] {
                width: 230px !important;
                margin: 0px auto 5px !important;
                float: none !important
            }
            [class="yes-no"] {
                vertical-align: top;
                padding-top: 9px
            }
            [class="member"] {
                width: 280px !important;
                margin-bottom: 20px !important
            }
            [class="client-img"] {
                width: 280px !important;
                margin-bottom: 20px !important
            }
            [class="client-text"] {
                width: 280px !important
            }
            [class="client-text"] td {
                text-align: center !important
            }
            [class="award"] {
                width: 280px !important;
                margin-bottom: 20px !important
            }
            [class="award"] img {
                margin: 0 auto
            }
            [class="none"] {
                height: 20px !important
            }
            [class="faq-text"] {
                width: 260px !important;
                clear: none
            }
            [class="blog-img"] {
                width: 280px !important
            }
            [class="blog-img"] img {
                width: 100% !important;
                height: auto !important
            }
            [class="blog-text"] {
                width: 280px !important;
                margin-bottom: 20px !important
            }
            [class="blog-text"] br {
                display: none !important
            }
            [class="col2"] {
                width: 280px !important;
                margin-bottom: 60px !important
            }
            [class="col2"] img {
                width: 100% !important;
                height: auto !important
            }
            [class="col2"] br {
                display: none !important
            }
            [class="contact"] {
                width: 280px !important;
                margin-bottom: 30px !important
            }
            [class="contact"] td {
                text-align: center !important
            }
            [class="copyright"] {
                width: 280px !important;
                margin-bottom: 30px !important
            }
            [class="footlink"] {
                width: 280px !important;
                margin-bottom: 30px !important
            }
            [class="footlink"] table {
                clear: none
            }
        }
    </style>
</head>

<body id="bd" style="margin:0; background:#fff;">
    <table bgcolor="005ec0" border="0" cellpadding="0" cellspacing="0" style="background: #005ec0 url(images/top-banner.jpg) repeat scroll center top / cover ; border-collaps:collaps; mso-table-lspace:0pt; mso-table-rspace:0pt;" width="100%" align="center">
        <tbody></tbody>
    </table>
    <table bgcolor="ffffff" border="0" cellpadding="0" cellspacing="0" style="border-collaps:collaps; mso-table-lspace:0pt; mso-table-rspace:0pt;" width="100%" align="center">
        <tbody>
            <tr>
                <td valign="top">
                    <table border="0" cellpadding="0" cellspacing="0" class="full" style="margin: 0 auto; border-collaps:collaps; mso-table-lspace:0pt; mso-table-rspace:0pt;" width="600" align="center">
                        <tbody>
                            <tr>
                                <td height="80"></td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%" cellspacing="0" cellpadding="0" border="0" align="left" style="mso-table-lspace:0pt; mso-table-rspace:0pt; margin: 0 auto; border-collaps:collaps;" class="mtitle">
                                        <tbody>
                                            <tr>
                                                <td height="40" style="letter-spacing: 1px; text-align: center; font-size:25px; font-family: 'Poppins' , 'arial'; font-weight:300 ; color: #5d5d5d;">Welcome {!! $name !!}, Thanks For Registering !</td>
                                            </tr>
                                            <tr>
                                                <td height="27"></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="f1f1f1" align="center" style="mso-table-lspace:0pt; mso-table-rspace:0pt; margin: 0 auto; border-collaps:collaps;">
                                                        <tbody>
                                                            <tr>
                                                                <td height="2"></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td height="43"></td>
                            </tr>
                            <tr>
                                <td style="text-align: center; font-size:16px; line-height: 28px; font-family: 'Poppins' , 'arial'; font-weight:300 ; color: #474747;">Please Click The Below Button To Verify Your E-Mail Address</td>
                            </tr>
                            <tr>
                                <td height="41"></td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="m_-5809257811666056298button m_-5809257811666056298large m_-5809257811666056298emails m_-5809257811666056298float-center" style="Margin:0 0 16px 0;border-collapse:collapse;border-spacing:0;float:none;margin-top:0;margin-bottom:16px;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;text-align:center;vertical-align:top;width:auto!important;text-align: center;margin: 0 auto;">
                        <tbody>
                            <tr style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;text-align:left;vertical-align:top">
                                <td style="Margin:0;background-color:#323358;background-image:none;background-repeat:repeat;background-position:top left;border-collapse:collapse!important;border-color:#323358;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:19px;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;text-align:left;vertical-align:top;word-wrap:break-word">
                                    <table style="border-collapse:collapse;border-spacing:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;text-align:left;vertical-align:top;width:100%">
                                        <tbody>
                                            <tr style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;text-align:left;vertical-align:top">
                                                <td style="Margin:0;background-color:#323358;background-image:none;background-repeat:repeat;background-position:top left;border-width:2px;border-style:solid;border-color:#323358;border-collapse:collapse!important;color:#fefefe;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:19px;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;text-align:left;vertical-align:top;word-wrap:break-word"><a href="{{url('/')}}/register/verify/{!! $token !!}" style="Margin:0;border-width:0;border-style:solid;border-color:#2199e8;border-radius:3px;color:#fefefe;display:inline-block;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:700;line-height:1.3;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:15px;padding-bottom:15px;padding-right:40px;padding-left:40px;text-align:left;text-decoration:none" target="_blank" data-saferedirecturl="{{url('/')}}/register/verify/{!! $token !!}">Verify</a></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <table bgcolor="ffffff" width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="border-collaps:collaps; mso-table-lspace:0pt; mso-table-rspace:0pt;">
        <tbody>
            <tr>
                <td width="100%">
                    <table width="600" cellspacing="0" cellpadding="0" border="0" align="center" style="margin: 0 auto; border-collaps:collaps; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full">
                        <tbody>
                            <tr>
                                <td>
                                    <table width="600" cellspacing="0" cellpadding="0" border="0" align="center" style="border-collaps:collaps; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full">
                                        <tbody>
                                            <tr>
                                                <td height="52"></td>
                                            </tr>
                                            <tr>
                                                <td valign="top">
                                                    <table class="copyright" border="0" cellpadding="0" cellspacing="0" style="mso-table-lspace:0pt; mso-table-rspace:0pt; margin: 0 auto; border-collaps:collaps;" width="auto" align="left">
                                                        <tbody>
                                                            <tr>
                                                                <td height="3"></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="text-align:center; font-size:15px; font-family:Poppins , arial; font-weight:300 ;height:17px; color: #474747;"> © 2017. All Rights Reserved.</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="53"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</body>
</html>