<?php

use Illuminate\Database\Seeder;

class OrderStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('order_status')->insert([
	    	[
	        'status' => 'Payment Processed'
	    	],
	    	[
	        'status' => 'Confirmed'
	    	],
	    	[
	        'status' => 'Refund In Process'
	    	],
	    	[
	        'status' => 'Refunded'
	    	],
	    	[
	        'status' => 'Cancelled'
	    	],
	    	[
	    	'status' => 'Delivered'
	    	]
		]);
    }
}
