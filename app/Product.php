<?php
namespace App;
use DB;
use Request;
use Validator;
use Auth;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use Illuminate\Database\Eloquent\Model;

class Product extends Model {
    
    public function category() {
        return $this->belongsTo('App\Category');
    }

    protected $fillable = ['name',
        'slug',
        'description',
        'price',
        'category_id',
        'image'
    ];
    
    public function createRules() {
        return $rules = [
            'name' => 'required|regex:/^[(a-zA-Z0-9\s)]+$/u|unique:products',
            'slug' => 'unique:products',
            'description' => 'required',
            'category_id' => 'required',
            'price' => 'required|numeric',
            'image' => 'required'
        ];
    }

    public function updateRules($id) {
        return $rules = [
            'name' => 'required|regex:/^[(a-zA-Z0-9\s)]+$/u|unique:products,name,'.$id,
            'description' => 'required',
            'slug' => 'unique:products,slug'.$id,
            'category_id' => 'required',
            'price' => 'required|numeric'
        ];
    }

    public function postProduct($request) {
        $product = new Product;
        $validator = Validator::make($request -> all(), $this -> createRules());
        if ($validator -> passes()) {
            /*Get Image, Move to Folder and Get Final Path*/
            $imgDestinationPath = 'uploads/products';
            $imgExtension = Request::file('image') -> getClientOriginalExtension();
            $productImage = Request::get('name').'.'.$imgExtension;
            Request::file('image') -> move($imgDestinationPath, $productImage);
            $imgPath = "/uploads/products/".$productImage;
            $product -> create(array('name' => Request::get('name'),
                'slug' => str_slug(Request::get('name'), '-'),
                'description' => Request::get('description'),
                'price' => Request::get('price'),
                'category_id' => Request::get('category_id'),
                'image' => $imgPath));
        } else {
            return $validator;
        }
    }

    public function updateProduct($id) {
        $product = Product::find($id);
        $validator = Validator::make(Request::all(), $this -> updateRules($id));
        if ($validator -> passes()) {
            $image = Input::file('image');
            if ($image) {
                $imgDestinationPath = 'uploads/products';
                $imgExtension = Request::file('image') -> getClientOriginalExtension();
                $productImage = Request::get('name').'.'.$imgExtension;
                Request::file('image') -> move($imgDestinationPath, $productImage);
                $imgPath = "/uploads/products/".$productImage;
            } else {
                $imgPath = $product->image;
            }
            $product -> fill(array('name' => Request::get('name'),
                'slug' => str_slug(Request::get('name'), '-'),
                'description' => Request::get('description'),
                'price' => Request::get('price'),
                'category_id' => Request::get('category_id'),
                'image' => $imgPath)) -> save();
        } else {
            return $validator;
        }
    }

    public function getMoreProducts($request){
        if (!$request->has('q')) {
            if ($request->has('proID') && $request->has('catID')) {
                $products = Product::where([['category_id', '=', $request->catID],['id', '>', $request->proID]])->limit(2)->get();
            }
            elseif ($request->has('proID')) {
                $products = Product::where('id','>',$request->proID)->limit(2)->get();
            }
        }
        else {
            if ($request->has('proID') && $request->has('catID')) {
                $ifProduct = Product::where('name', 'like', '%'.$request->q.'%')->get();
                if($ifProduct->count() > 0)
                {
                    $products = Product::where('name', 'like', '%'.$request->q.'%')->where('category_id',$request->catID)->where('id','>',$request->proID)->limit(2)->get();
                }
                else{
                    $products = Product::whereHas('category', function($query) use($request){
                    $query->where('name', 'like', '%'.$request->q.'%');})->where('category_id',$request->catID)->where('id','>',$request->proID)->limit(2)->get();
                }
            }
            elseif ($request->has('proID')) {
                $ifProduct = Product::where('name', 'like', '%'.$request->q.'%')->get();
                if($ifProduct->count() > 0){
                    $products = Product::where('name', 'like', '%'.$request->q.'%')->where('id','>',$request->proID)->limit(2)->get();    
                }
                else{
                    $products = Product::whereHas('category', function($query) use($request){
                    $query->where('name', 'like', '%'.$request->q.'%');})->where('id','>',$request->proID)->limit(2)->get();
                }
            }
        }
        return $products;
    }

    public function getFilteredProducts($request){
        if($request->has('q')) {
            $ifProduct = Product::where('name', 'like', '%'.$request->q.'%')->get(); 
            if($ifProduct->count() > 0)
            {
                $products = Product::where([['category_id', '=', $request->id],['name', 'like', '%'.$request->q.'%']])->limit('4')->get();
            }
            else{
                $products = Product::whereHas('category', function($query) use($request){
                $query->where('id',$request->id);})->limit('4')->get();
            }
        }
        else{
            $products = Product::where('category_id',$request->id)->limit(4)->get();     
        }
    return $products;
    }

    public function search($q){
        $data['products'] = Product::whereHas('category', function($query) use($q) {
            $query->where('name', 'like', '%'.$q.'%');})
                ->orWhere('name', 'LIKE', '%'.$q.'%')->limit('4')->get();
        $data['categories'] = Category::whereHas('product', function($query) use($q) {
            $query->where('name', 'like', '%'.$q.'%');})
                ->orWhere('name', 'LIKE', '%'.$q.'%')->get();
        return $data;
    }
}