<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Product;
use App\Category;
use App\Order;
use App\Cart;
use Auth;
use Response;
use Illuminate\Support\Facades\Input;
use Session;
use Redirect;
use Exception;
use Stripe\Charge;
use Stripe\Stripe;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class ProductController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $products = Product::all();
        $categories = Category::all()->pluck('name');
        return view('admin.products')->with(array('products'=>$products,'categories'=>$categories,'entity'=>"products"));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {}
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $input = $request->all();
        $product = new Product;
        $validator = $product->postProduct($request);
        if ($validator) {
            return $validator->messages();
        } else {
            return "success";
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id) {
        if ($product = Product::find($id)) {
            return $product;
        } else {
            return "Product doesn't exist";
        }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {}
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $product = new Product;
        $validator = $product->updateProduct($id);

        if ($validator) {
            return redirect()->back()->with('errors', $validator->messages())->with('error_code', 'PUE');
        } else {
            return redirect('/admin/products');
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $product = Product::findOrFail($id);
        if ($product->delete()) {
            Session::forget('cart');
            return "success";
        }
    }
    /**
     * Get the ID of Product From Route and Add To Cart.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getAddToCart(Request $request, $slug) {
        try {
        $product = Product::where('slug', $slug)->first();
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->add($product,$product->id);
        if (Session::put('cart', $cart)) {
            return "success";
        }   
        } catch (Exception $e) {
            return $e;
        }
    }
    public function deleteFromCart($slug) {
        $product = Product::where('slug', $slug)->first();
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->remove($cart, $product->id);
        Session::put('cart', $cart);
        if (Session::get('cart')->totalQty<1) {
            Session::forget('cart');
        }
        return redirect()->back();
    }
    public function showCart(Request $request) {
        if (!Session::has('cart')) {
            $products = null;
            return view('site.shop.cart')->with('products', $products);
        }
        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
        return view('site.shop.cart')->with(array('products'=>$cart->items,'totalPrice'=>$cart->totalPrice));
    }
    public function getCart(Request $request) {
        if (!Session::has('cart')) {
            return response()->json("Empty Cart");
        }
        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
        return response()->json(['cart'=>$cart->items,'totalQty' =>$cart->totalQty]);
    }
    public function checkout() {
        if (!Session::has('cart')) {
            $products = null;
            return view('site.shop.checkout')->with('products', $products);
        }
        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
        return view('site.shop.checkout')->with(array('customer' => Session::get('customer'),'products'=>$cart->items,'totalPrice' => $cart->totalPrice));
    }

    public function postCheckout(Request $request) {
        if (!Session::has('cart')) {
            return redirect('/products');
        }
        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
        $token = $request->input('stripeToken');
        Stripe::setApiKey(env('STRIPE_KEY'));
        try {
            $order = new Order();
            $validator = $order->newOrder($cart, $token);
            if (!$validator) {
                return redirect('/my-account');
            } else {
                $charge = $validator;
            }
        } catch (Exception $e) {
            return $e;
        }
        return redirect("/order-complete/$charge->id");
    }

    public function orderSuccess($token) {
        Stripe::setApiKey(env('STRIPE_KEY'));
        $charge = Charge::retrieve($token);
        $order = Order::findOrFail($charge->metadata->order_id);
        $cart = unserialize($order ->cart);
        return view('site.shop.checkout_success')->with(array('token' =>$token,'products' =>$cart->items,'totalPrice'=>$cart->totalPrice));
    }

    public function search(Request $request) {
        $product = new Product;
        $q = $request->input('s');
        $data = $product->search($q);
        return view('site.search')->with(array('query'=>$q,'products'=>$data['products'],'categories'=>$data['categories']));
    }

    public function renderProductsView($products) {
        if(!$products->isEmpty())
        {
        $render = ' ';
        foreach($products as $product) {
            $pslug = "/products/$product->slug";
            $pimg = $product->image;
            $pname = $product->name;
            $pprice = $product->price;
            $pcategory = $product->category->name;
            $pdesc = $product->description;
            $render.= "<div class='col-md-6 col-sm-6 wow zoomIn' data-wow-delay='0.1s'><a class='strip_list grid' href='$pslug'><div class='desc'><div class='thumb_strip'><img src='$pimg' alt=''></div><div class='rating'><i class='icon_star voted'></i><i class='icon_star voted'></i><i class='icon_star voted'></i><i c6lass='icon_star voted'></i><i class='icon_star'></i></div><h3>$pname</h3><div class='type'>$pcategory</div><div><h4>$$pprice</h4></div><div class='location'>$pdesc</div><ul><li>Delivery<i class='icon_check_alt2 ok'></i></li></ul></div></a></div>";
            $id =$product->id;
        }
        return response()->json([
            'products' => $render,
            'id' => $id
        ]);    
        }
        else{
            return $products;
        }        
    }

    public function loadProducts(Request $request)
    {
        $product  = new Product;
        $products = $product->getMoreProducts($request);
        return $this->renderProductsView($products);
    }

    public function filterProducts(Request $request){
        $product = new Product;
        $products = $product->getFilteredProducts($request);
        return $this->renderProductsView($products);
    }
}