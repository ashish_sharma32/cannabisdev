<?php

namespace App\Http\Controllers;

use Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Product;
use Illuminate\Support\Facades\Input;
use Session;
use Redirect;

class CategoryController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $categories = Category::all();
        return view('admin.categories')->with(array('categories'=>$categories,'entity'=>"categories"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('admin.categories_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $category = new Category;
        $validator = $category->postCategory();    
        if($validator){
            return $validator->errors()->all();
        }
        else{
            return "success";
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        if ($category = Category::find($id)) {
            return $category;
        } else {

            return "Category don't exist";
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        if ($category = Category::where('id', $id) -> first()) {
            return view('admin.categories_edit') -> with('category', $category);
        } else {
            return "Product Don't Exist";
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $input = Request::all();
        $category = new Category;
        $validator = $category -> updateCategory($input, $id);

        if ($validator) {
            return redirect() -> back() -> with('errors', $validator -> messages()) -> with('error_code', 'CUE');
        } else {
            return redirect() -> back();
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */ 
    public function destroy($id) {

        if (Product::where('category_id', '=', $id) -> exists()) {
            return "error";
        } else {
            $category = Category::findOrFail($id);
            $category->delete();
            return "success";            
        }
    }
}