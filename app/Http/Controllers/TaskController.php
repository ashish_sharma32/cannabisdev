<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\User;
use Response;
use Exception;
use App\Task;

class TaskController extends Controller
{
	public function index(){
		try {
		$client = new Client();
		$body = $client -> request('POST', 'https://api.tookanapp.com/v2/get_all_tasks', [
		'form_params' => [
		'api_key' => env('TOOKANAPP_KEY'),
		'job_type' => 1
		]
		])->getBody();		
		$tasks = json_decode($body);
		return view('admin.tasks')->with('tasks',$tasks->data);
		} catch (Exception $e) {
		return Response::json('Some Errors Occoured', 503);
		}
	}

	public function show($id,Request $request){
		try {
				$client = new Client();
				$body = $client -> request('POST', 'https://api.tookanapp.com/v2/get_task_details_by_order_id', [
					'form_params' => [
					'api_key' => env('TOOKANAPP_KEY'),
					'order_id' => $id,
					'user_id' => env('TOOKANAPP_USERID')
				]
				])->getBody();
				return $body;
			} catch (Exception $e) {
					return response($e);
			}
	}	
}
