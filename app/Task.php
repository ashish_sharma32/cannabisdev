<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Session;
use Auth;
use Exception;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Client;

class Task extends Model
{
    public function user(){
    	return $this->belongsTo('App\User');
    }

    public function newTask($order)
    { 
		$client = new Client();
		try {
			$task = $client -> request('POST', 'https://api.tookanapp.com/v2/create_task', [
			'form_params' => [
			'api_key' => env('TOOKANAPP_KEY'),
			'order_id' => $order->id,
			'job_description' => 'Test Delivery',
			'customer_email' => Session::get('customer')['email'],
			'customer_username' => Session::get('customer')['firstname'] . ' ' . Session::get('customer')['lastname'],
			'customer_phone' => '+'. Session::get('customer')['country_code'] . Session::get('customer')['telephone'],
			'customer_address' => Session::get('customer')['address_street'] . ' ' . Session::get('customer')['address_city'],
			'job_delivery_datetime' => '2017-12-12 21:00:00',
			'custom_field_template' => 'cannabis delivery',
			'team_id' => '26215',
			'auto_assignment' => '1',
			'has_pickup' => '0',
			'has_delivery' => '1',
			'layout_type' => '0',
			'tracking_link' => '1',
			'timezone' => '-330',
			'ref_images'=> ['http://cannabis.devziners.com/uploads/products/Product%201.jpg'],
			'notify' => '1',
			'geofence' => '1'
			]])->getBody();
		$body = json_decode($task);
		return $body->status;	
		} catch (Exception $e) {
			dd($e);
		}	
    }
}