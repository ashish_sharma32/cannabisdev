<?php 
namespace App;
use DB;
use Request;
use Validator;
use Auth;
use Mail;
use App\Http\Requests;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable {
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable=[ 'firstname',
    'lastname',
    'email',
    'telephone',
    'address_street',
    'address_city',
    'address_zip',
    'country',
    'verified',
    'password',
    'admin',
    'remember_token',
    'provider',
    'provider_id'
    ];
    public function orders(){
        return $this->hasMany('App\Order');
    }

    public function setPasswordAttribute($password)
    {   
        $this->attributes['password'] = bcrypt($password);
    } 

    public function rules()
    {
        return $rules = [
        'firstname'=> 'required|min:2|alpha',
        'lastname'=> 'required|min:2|alpha',
        'email' => 'required|email|unique:users',
        'password'=> 'required|regex:/^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$/|between:8,12|confirmed',
        'password_confirmation'=> 'required',
        'token' => 'unique:users',
        'telephone'=> 'required|between:10,12',
        'admin'=> 'integer'
        ];
    }

    public function updateRules($id)
    {
        return $rules = [
        'firstname'=> 'required|min:2|alpha',
        'lastname'=> 'required|min:2|alpha',
        'email' => 'required|email|unique:users,email,'.$id,
        'token' => 'unique:users',
        'telephone'=> 'required|between:10,12'
        ];
    }

    public function passwordRules()
    {
        return $rules = [
        'password'=> 'required|regex:/^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$/|between:8,12|confirmed',
        'password_confirmation'=> 'required'
        ];
    }

    public function loginRules()
    {
        return $rules = [
        'email'=> 'required|email',
        'password'=> 'required|regex:/^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$/|between:8,12'
        ];
    }


    /**
     * The attributes that should be gaurded for arrays.
     *
     * @var array
     */
    protected $hidden=[ 'password','remember_token','admin','token'];

    public function postUser() {
        $user = new User;
        $validator = Validator::make(Request::all(), $this->rules());
        if($validator->passes())
        {
            if(User::create(Request::all()))
            {
                $user = User::where('email', Request::get('email'))->first();
                $user->token = str_random(30);
                $user->save();
                $name = Request::get('firstname');
                $data = array(
                'name' => $name,
                'token' => $user->token,
                );
                Mail::send('emails.welcome', $data, function ($message) {
                    $email = Request::get('email');
                    $message->from('info@ashish-sharma.net', 'Mail Verification System');
                    $message->to($email)->subject('Welcome Mail');
                });    
            }   
        }
        else
        {
            return $validator;
        }
    }
    public function updateUser($id,$input){
        if($user = User::findorfail($id))
        {
            $validator = Validator::make($input, $this->updateRules($id));
            if($validator->passes())
            {
                $user->fill($input)->save();
            }
            else
            {
                return $validator;
            }
        }
    }
    public function updatePassword($id,$input)
    {
        if($user = User::findorfail($id))
        {
            $validator = Validator::make($input, $this->passwordRules());
            if($validator->passes())
            {
                $user->fill($input)->save();
            }
            else
            {
                return $validator;
            }
        }
    }
    public function loginUser() {
        $validator = Validator::make(Request::all(), $this->loginRules());
        if(!$validator->passes())
        {   
            return $validator;
        }

    }
}