$(function() {
    myform = $("#myform"), myform.validate({
        rules: {
            firstname: {
                required: !0,
                minlength: 3
            },
            lastname: {
                required: !0,
                minlength: 3
            },
            password: {
                minlength: 8,
                alphanumeric: !0
            },
            password_confirmation: {
                minlength: 8,
                alphanumeric: !0,
                equalTo: "#password"
            }
        },
        submitHandler: function(e) {
            $spinner = "<div class='spinner'><div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div>", $("button#submit").html($spinner);
            var formData = new FormData(this);
            $.ajax({
                type: "post",
                url: "/user/store",
                data: formData,
                success: function(e) {
                    "success" == e ? ($("#register").modal("toggle"), swal("Congrats", "Your Account Has Been Successfully Created", "success"), $("button#submit").html("Register"), $("#myform").trigger("reset")) : ($("#register").modal("toggle"), $("button#submit").html("Register"), $("#myform").trigger("reset"))
                }
            })
        }
    }), $("#email").bind("change paste keyup", function() {
        $this = $(this);
        var e = $this.val(),
            t = $("#id").val(),
            a = $("input[name=_token]").val(),
            s = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        s.test(e) && $.ajax({
            type: "post",
            url: "/checkmail",
            data: {
                email: e,
                _token: a,
                id: t
            },
            success: function(e) {
                "no" == e.allowed ? ($("#mail_error").html("Email Address Already exists !").show(), $("button#submit").prop("disabled", !0)) : ($("#mail_error").hide(), $("button#submit").prop("disabled", !1))
            }
        })
    })
});